package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.controller;

import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity.Certification;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.repository.CertificationRepository;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.request.CertificationRequest;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.response.CertificationResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@RestController
public class CertificationController {

    @Autowired
    private CertificationRepository certificationRepository;

    @PostMapping("/insertCertification")
    public ResponseEntity<?> editCertification(@RequestBody CertificationRequest certificationRequest) {

        List<Certification> certifications = new ArrayList<>();
        Certification certification = new Certification();

        certification.setCertifications_id(certificationRequest.getCertifications_id());
        certification.setCertificationtype(certificationRequest.getCertificationtype());
        certification.setCertificationname(certificationRequest.getCertificationname());
        certification.setCertificationowner(certificationRequest.getCertificationowner());
        certification.setCertificationdate(certificationRequest.getCertificationdate());
        certification.setCertificationexpiredate(certificationRequest.getCertificationexpiredate());
        certification.setStatus(1);

        certifications.add(certification);
        certificationRepository.saveAll(certifications);

        return ResponseEntity.ok(new CertificationResponse(1));

    }

    @GetMapping("/getCertifications")
    public List<Certification> getCertifications() {

        return certificationRepository.findAll();

    }

    @GetMapping(path = "/getCertificationByID/{id}")
    public Certification getCertifications(@PathVariable("id") int id) {

        return certificationRepository.findAllById(id).get(0);

    }

    @PostMapping("/updateCertification")
    public int updateCertification(@RequestBody CertificationRequest request) {

        certificationRepository.updateCertification(request
                .getId(), request.getCertifications_id(), request.getCertificationtype(), request.getCertificationname(), request.getCertificationowner(), request.getCertificationdate(),request.getCertificationexpiredate(),request.getStatus() );

        return 1;

    }

    @PostMapping("/deleteCertification")
    public int deleteCertification(@RequestBody CertificationRequest request) {

        certificationRepository.updateStatusById(request.getId());

        return 1;

    }
}
