package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.export;

import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity.Course;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class CourseExcelExporter {

    private XSSFWorkbook workbook;
    private XSSFSheet sheet;

    private List<Course> listCourses;
    private int rowIndex = 1;

    public CourseExcelExporter(List<Course> listCourses) {
        this.listCourses = listCourses;
        workbook = new XSSFWorkbook();
        sheet = workbook.createSheet("Kurse");

    }

    private void writeHeaderRow(){

        XSSFFont font= workbook.createFont();
        font.setBold(true);

        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setFont(font);

        Row row = sheet.createRow(0);

        Cell cell = row.createCell(0);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Angebotsnummer");

        cell = row.createCell(1);
        cell.setCellValue("Angebot");

        cell = row.createCell(2);
        cell.setCellValue("Kursname");

        cell = row.createCell(3);
        cell.setCellValue("Veranstaltungsart");

        cell = row.createCell(4);
        cell.setCellValue("Kursdatum");

        cell = row.createCell(5);
        cell.setCellValue("Thema");

        cell = row.createCell(6);
        cell.setCellValue("Kursort");

        cell = row.createCell(7);
        cell.setCellValue("Begleitende Lehrkraft");

        cell = row.createCell(8);
        cell.setCellValue("Betreunde Studenten");

        cell = row.createCell(9);
        cell.setCellValue("Anzahl der Anwesenden");

        cell = row.createCell(10);
        cell.setCellValue("Anzahl der Angemeldeten");

        cell = row.createCell(11);
        cell.setCellValue("Anzahlung");

        cell = row.createCell(12);
        cell.setCellValue("Honorar");

        cell = row.createCell(13);
        cell.setCellValue("Kursleiter");

        cell = row.createCell(14);
        cell.setCellValue("Aktiv");

        cell = row.createCell(15);
        cell.setCellValue("Notiz");

    }

    private void writeDateRows(){

        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setAlignment(HorizontalAlignment.CENTER);

        for (Course course: listCourses) {

            Row row = sheet.createRow(rowIndex);
            rowIndex++;

            Cell cell = row.createCell(0);
            cell.setCellValue(course.getCourse_number());

            cell = row.createCell(1);
            cell.setCellValue(course.getCourse_offer());

            cell = row.createCell(2);
            cell.setCellValue(course.getCourse_name());

            cell = row.createCell(3);
            cell.setCellValue(course.getEvent_type());

            cell = row.createCell(4);
            cell.setCellValue(course.getCourse_datetime());

            cell = row.createCell(5);
            cell.setCellValue(course.getCourse_topic());

            cell = row.createCell(6);
            cell.setCellValue(course.getCourse_location());

            cell = row.createCell(7);
            cell.setCellValue(course.getAccompanyingTeacher());

            cell = row.createCell(8);
            cell.setCellValue(course.getSupervisingStudents());

            cell = row.createCell(9);
            cell.setCellStyle(cellStyle);
            cell.setCellValue(course.getPresentStudents());

            cell = row.createCell(10);
            cell.setCellStyle(cellStyle);
            cell.setCellValue(course.getRegisteredStudents());

            cell = row.createCell(11);
            cell.setCellValue(course.getAnzahlung());

            cell = row.createCell(12);
            cell.setCellValue(course.getHonorar());

            cell = row.createCell(13);
            cell.setCellValue(course.getInstructor());

            cell = row.createCell(14);
            cell.setCellStyle(cellStyle);
            cell.setCellValue((course.getStatus() == 1) ? "Ja" : "Nein");

            cell = row.createCell(15);
            cell.setCellValue(course.getNoteField());
        }
        sheet.autoSizeColumn(0);
        sheet.autoSizeColumn(1);
        sheet.autoSizeColumn(2);
        sheet.autoSizeColumn(3);
        sheet.autoSizeColumn(4);
        sheet.autoSizeColumn(5);
        sheet.autoSizeColumn(6);
        sheet.autoSizeColumn(7);
        sheet.autoSizeColumn(8);
        sheet.autoSizeColumn(9);
        sheet.autoSizeColumn(10);
        sheet.autoSizeColumn(11);
        sheet.autoSizeColumn(12);
        sheet.autoSizeColumn(13);
        sheet.autoSizeColumn(14);
        sheet.autoSizeColumn(15);

    }

    public ResponseEntity<InputStreamResource> export() throws IOException {
        writeHeaderRow();
        writeDateRows();

        File file = new File(getClass().getResource(".").getFile() + "/courses.xlsx");
        file.createNewFile();
        FileOutputStream outputStream = new FileOutputStream(file, false);
        workbook.write(outputStream);
        workbook.close();
        outputStream.close();

        String headerKey = "Content-Disposition";
        String headerValue = "attachement; filename=kurse.xlsx";

        InputStreamResource resource = new InputStreamResource(new FileInputStream(file));

        return ResponseEntity.ok().header(headerKey, headerValue)
                .contentLength(file.length())
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(resource);
    }

}
