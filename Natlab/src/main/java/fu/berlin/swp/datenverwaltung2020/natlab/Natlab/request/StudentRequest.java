package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class StudentRequest {

    private Integer id;
    private String  anrede;
    private String  firstname;
    private String  lastname;
    private String  street;
    private String  postcode;
    private String  ort;
    private String  sommeruni_17;
    private String  sommeruni_18;
    private String  sommeruni_19;
    private String  sommeruni_20;
    private String  natuerlich_2020;
    private int status;

}