package fu.berlin.swp.datenverwaltung2020.natlab.Natlab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NatlabApplication {

	public static void main(String[] args) {

		SpringApplication.run(NatlabApplication.class, args);

	}

}
