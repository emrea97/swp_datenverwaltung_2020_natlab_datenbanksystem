package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.repository;

import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity.Certification;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

public interface CertificationRepository extends JpaRepository<Certification, Integer> {

    List<Certification>  findAllById(int cid);

    @Transactional @Modifying
    @Query("FROM Certification c WHERE c.status = 1")
    List<Certification> findAll();

    @Transactional @Modifying
    @Query("UPDATE Certification c SET c.certifications_id = :certifications_id, c.certificationtype = :certificationtype, c.certificationname = :certificationname, c.certificationowner = :certificationowner, c.certificationdate = :certificationdate, c.certificationexpiredate = :certificationexpiredate, c.status = :status WHERE c.id = :id")
    void updateCertification(@Param("id") int id, @Param("certifications_id") String certifications_id, @Param("certificationtype") String certificationtype, @Param("certificationname") String certificationname, @Param("certificationowner") String certificationowner, @Param("certificationdate") String certificationdate, @Param("certificationexpiredate") String certificationexpiredate, @Param("status") int status);

    @Transactional @Modifying
    @Query("UPDATE Certification c SET c.status = 0 WHERE c.id = :id")
    void updateStatusById(@Param("id") Integer id);

}