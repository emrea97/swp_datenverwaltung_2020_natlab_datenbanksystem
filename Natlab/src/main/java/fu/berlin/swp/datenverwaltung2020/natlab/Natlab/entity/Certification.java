package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data @Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "certifications")
public class Certification {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "cid")
    private int id;

    @Column(name = "certifications_id")
    private String certifications_id;

    @Column(name = "certificationtype")
    private String certificationtype;

    @Column(name = "certificationname")
    private String certificationname;

    @Column(name = "certificationowner")
    private String certificationowner;

    @Column(name = "certificationdate")
    private String certificationdate;

    @Column(name = "certificationexpiredate")
    private String certificationexpiredate;

    @Column(name = "status")
    private int status;

}
