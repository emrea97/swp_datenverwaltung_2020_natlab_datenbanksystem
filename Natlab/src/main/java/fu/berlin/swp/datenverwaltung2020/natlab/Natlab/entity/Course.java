package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "courses")
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "course_id")
    private Integer id;

    @Column(name = "course_number")
    private String course_number;

    @Column(name = "course_offer")
    private String course_offer;

    @Column(name = "course_name")
    private String course_name;

    @Column(name = "event_type")
    private String event_type;

    @Column(name = "course_date")
    private String course_datetime;

    @Column(name = "course_topic")
    private String course_topic;

    @Column(name = "course_location")
    private String course_location;

    @Column(name = "accompanying_teacher")
    private String accompanyingTeacher;

    @Column(name = "supervising_students")
    private String supervisingStudents;

    @Column(name = "present_students")
    private int presentStudents;

    @Column(name = "registered_students")
    private int registeredStudents;

    @Column(name = "instructor")
    private String instructor;

    @Column(name = "note_field")
    private String noteField;

    @Column(name = "anzahlung")
    private String anzahlung;

    @Column(name = "honorar")
    private String honorar;

    @Column(name = "status")
    private int status;

    @ManyToMany
    @JoinTable(name = "users_courses",
            joinColumns = @JoinColumn(name = "course_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    private List<User> users;

    @ManyToMany
    @JoinTable(name = "teachers_courses",
            joinColumns = @JoinColumn(name = "course_id"),
            inverseJoinColumns = @JoinColumn(name = "teacher_id"))
    private List<Teacher> teachers;

    @ManyToMany
    @JoinTable(name = "lectureships_courses",
            joinColumns = @JoinColumn(name = "course_id"),
            inverseJoinColumns = @JoinColumn(name = "ifd_nr"))
    private List<Lectureship> lectureships;

    @ManyToMany
    @JoinTable(name = "calendars_courses",
            joinColumns = @JoinColumn(name = "course_id"),
            inverseJoinColumns = @JoinColumn(name = "date_id"))
    private List<Calendar> calendars;

}
