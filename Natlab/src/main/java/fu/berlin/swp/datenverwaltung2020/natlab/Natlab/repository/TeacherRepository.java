package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.repository;

import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity.Course;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface TeacherRepository extends JpaRepository<Teacher, Integer> {

    List<Teacher>  findAllById(int id);

    @Transactional @Modifying
    @Query("FROM Teacher t WHERE t.status = 1")
    List<Teacher> findAll();

    @Transactional @Modifying
    @Query("UPDATE Teacher t SET t.school_branch = :school_branch, t.grad = :grad, t.firstname = :firstname, t.lastname = :lastname, t.gender = :gender, t.subject = :subject, t.personal_number = :personal_number, t.email = :email, t.school_name = :school_name, t.school_number = :school_number, t.telephone = :telephone, t.street = :street, t.zip = :zip, t.city = :city, t.status = :status WHERE t.id = :teacherId")
    void updateTeacher(@Param("teacherId") int id, @Param("school_branch") String schoolBranch, @Param("grad") String grad, @Param("firstname") String firstname, @Param("lastname") String lastname, @Param("gender") String gender, @Param("subject") String subject, @Param("personal_number") String personal_number, @Param("email") String email, @Param("school_name") String school_name, @Param("school_number") int school_number, @Param("telephone") String telephone, @Param("street") String street, @Param("zip") int zip, @Param("city") String city, @Param("status") int status );

    @Transactional @Modifying
    @Query("UPDATE Teacher t SET t.status = 0 WHERE t.id = :teacherId")
    void updateStatusById(@Param("teacherId") Integer id);

}