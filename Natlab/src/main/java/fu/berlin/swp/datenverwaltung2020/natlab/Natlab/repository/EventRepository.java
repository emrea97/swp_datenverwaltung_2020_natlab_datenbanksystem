package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.repository;

import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface EventRepository extends JpaRepository<Event, Integer> {

    @Transactional @Modifying
    @Query("FROM Event e WHERE e.status = 1")
    List<Event> findAll();

    @Transactional @Modifying
    @Query("UPDATE Event e SET e.status = 0 WHERE e.id = :eventID")
    void updateStatusById(@Param("eventID") Integer id);

}
