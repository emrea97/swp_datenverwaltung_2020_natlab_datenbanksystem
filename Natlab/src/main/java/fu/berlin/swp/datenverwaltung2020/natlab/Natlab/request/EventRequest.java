package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.request;

import lombok.*;

@Data
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class EventRequest {

    private int id;
    private String title;
    private String description;
    private String startTime;
    private String endTime;
    private int status;

}
