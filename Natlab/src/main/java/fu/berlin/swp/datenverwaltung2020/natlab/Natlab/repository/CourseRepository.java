package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.repository;

import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity.Course;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

@Repository
public interface CourseRepository extends JpaRepository<Course, Integer> {

    List<Course> findAllById(int id);

    @Transactional @Modifying
    @Query("FROM Course c WHERE c.status = 1")
    List<Course> findAll();

    @Transactional @Modifying
    @Query("UPDATE Course c SET c.course_number = :courseNumber, c.course_offer = :courseOffer, c.course_name = :name, c.event_type = :eventType, c.course_datetime = :courseDate, c.course_topic = :topic, c.course_location = :location, c.accompanyingTeacher = :accompanyingTeacher, c.supervisingStudents = :supervisingStudents, c.presentStudents = :presentStudents, c.registeredStudents = :registeredStudents, c.instructor = :instructor,  c.honorar = :honorar,  c.anzahlung = :anzahlung, c.noteField = :noteField WHERE c.id = :courseId")
    void updateCourse(@Param("courseId") int id, @Param("courseNumber") String courseNumber, @Param("courseOffer") String courseOffer, @Param("name") String name, @Param("eventType") String eventType, @Param("courseDate") String courseDate, @Param("topic") String topic, @Param("location") String location, @Param("accompanyingTeacher") String accompanyingTeacher, @Param("supervisingStudents") String supervisingStudents, @Param("presentStudents") int presentStudents, @Param("registeredStudents") int registeredStudents, @Param("instructor") String instructor,  @Param("honorar") String honorar,  @Param("anzahlung") String anzahlug, @Param("noteField") String noteField );

    @Transactional @Modifying
    @Query("UPDATE Course c SET c.status = 0 WHERE c.id = :courseId")
    void updateStatusById(@Param("courseId") Integer id);

}
