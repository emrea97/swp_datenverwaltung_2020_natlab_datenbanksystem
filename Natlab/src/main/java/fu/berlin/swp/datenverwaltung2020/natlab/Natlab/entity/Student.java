package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "students")
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "anrede")
    private String anrede;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "lastname")
    private String lastname;

    @Column(name = "street")
    private String street;

    @Column(name = "postcode")
    private String postcode;

    @Column(name = "ort")
    private String ort;

    @Column(name = "summeruni_17")
    private String summerUni_17;

    @Column(name = "summeruni_18")
    private String summerUni_18;

    @Column(name = "summeruni_19")
    private String summerUni_19;

    @Column(name = "sommeruni_20")
    private String summerUni_20;

    @Column(name = "natuerlich_2020")
    private String natuerlich;

    @Column(name = "status")
    private int status;


    @ManyToMany
    @JoinTable(name = "users_students",
            joinColumns = @JoinColumn(name = "student_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    private List<User> users;

    @ManyToMany
    @JoinTable(name = "teachers_students",
            joinColumns = @JoinColumn(name = "student_id"),
            inverseJoinColumns = @JoinColumn(name = "teacher_id"))
    private List<Teacher> teachers;

}
