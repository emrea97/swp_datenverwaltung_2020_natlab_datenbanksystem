package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.response;

import lombok.Getter;

@Getter
public class CourseResponse {

    private int status;

    public CourseResponse(int status) {
        this.status = status;

    }

}
