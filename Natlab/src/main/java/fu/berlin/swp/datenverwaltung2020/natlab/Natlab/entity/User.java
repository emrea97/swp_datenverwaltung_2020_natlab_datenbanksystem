package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "users_id")
    private int id;

    @Column(name = "userName")
    private String userName;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @ManyToMany(mappedBy = "users")
    private List<Course> courses;

    @ManyToMany(mappedBy = "users")
    private List<Teacher> teachers;

    @ManyToMany(mappedBy = "users")
    private List<Lectureship> lectureships;

    @ManyToMany(mappedBy = "users")
    private List<Student> students;

    @ManyToMany(mappedBy = "users")
    private List<Calendar> calendars;

    public User(String firstname, String name, String userName, String email, String password) {
        this.firstName = firstname;
        this.lastName = name;
        this.userName = userName;
        this.email = email;
        this.password = password;
    }

    public String getFirstname() {
        return firstName;
    }

    public String getName() {
        return lastName;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public int getId() { return id; }
}

