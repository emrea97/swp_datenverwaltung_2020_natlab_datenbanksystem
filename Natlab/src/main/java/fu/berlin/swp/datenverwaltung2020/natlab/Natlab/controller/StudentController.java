package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.controller;

import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity.Student;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.export.StudentExcelExporter;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.repository.StudentRepository;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.request.StudentRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
public class StudentController {

    @Autowired
    private StudentRepository studentRepository;

    @PostMapping("/insertStudent")
    public ResponseEntity<?> insertStudent(@RequestBody StudentRequest request) {
        List<Student> students = new ArrayList<>();
        Student student = new Student();
        student.setAnrede(request.getAnrede());
        student.setFirstName(request.getFirstname());
        student.setLastname(request.getLastname());
        student.setStreet(request.getStreet());
        student.setPostcode(request.getPostcode());
        student.setOrt(request.getOrt());
        student.setSummerUni_17(request.getSommeruni_17());
        student.setSummerUni_18(request.getSommeruni_18());
        student.setSummerUni_19(request.getSommeruni_19());
        student.setSummerUni_20(request.getSommeruni_20());
        student.setNatuerlich(request.getNatuerlich_2020());
        student.setStatus(1);

        students.add(student);

        studentRepository.saveAll(students);

        return ResponseEntity.ok(student);

    }

    @GetMapping("/getStudents")
    public List<Student> getStudents() {

        return studentRepository.findAll();

    }

    @GetMapping(path = "/getStudentByID/{id}")
    public Student getStudent(@PathVariable("id") int id) {

        return studentRepository.findAllById(id).get(0);

    }

    @PostMapping("/updateStudent")
    public int updateStudent(@RequestBody StudentRequest request) {

        studentRepository.updateStudent(request.getId(), request.getAnrede(), request.getFirstname(), request.getLastname(), request.getStreet(),request.getPostcode(),request.getOrt(), request.getSommeruni_17(), request.getSommeruni_18(), request.getSommeruni_19(), request.getSommeruni_20(), request.getNatuerlich_2020(), request.getStatus());

        return 1;

    }

    @PostMapping("/deleteStudent")
    public ResponseEntity<?> deleteStudent(@RequestBody StudentRequest request) {

        studentRepository.updateStatusById(request.getId());

        return ResponseEntity.ok(1);

    }

    @GetMapping(path = "/exportStudentExcel/{ids}")
    public ResponseEntity<?> exportExcel(@PathVariable("ids") String[] ids) throws IOException {

        File file = new File(getClass().getResource(".").getFile() + "/Studenten.xlsx");
        file.createNewFile();
        List<Student> students;

        ArrayList<Integer> servs = new ArrayList<>();
        for (String id : ids) {
            servs.add(Integer.parseInt(id));
        }

        students = studentRepository.findAllById(servs);

        StudentExcelExporter excelExporter = new StudentExcelExporter(students);

        return excelExporter.export();

    }

    @GetMapping(path = "/exportStudentExcel")
    public ResponseEntity<?> exportExcelAll() throws IOException {

        File file = new File(getClass().getResource(".").getFile() + "/studenten.xlsx");
        file.createNewFile();
        List<Student> students = studentRepository.findAll();

        StudentExcelExporter excelExporter = new StudentExcelExporter(students);

        return excelExporter.export();

    }

}
