package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.repository;

import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity.Lectureship;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LectureshipRepository extends JpaRepository<Lectureship, Integer> {
}