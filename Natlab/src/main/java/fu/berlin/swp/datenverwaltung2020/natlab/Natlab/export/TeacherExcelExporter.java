package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.export;

import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity.Course;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity.Teacher;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class TeacherExcelExporter {

    private final XSSFWorkbook workbook;
    private final XSSFSheet sheet;

    private final List<Teacher> listTeacher;
    private int rowIndex = 1;

    public TeacherExcelExporter(List<Teacher> listTeacher) {
        this.listTeacher = listTeacher;
        workbook = new XSSFWorkbook();
        sheet = workbook.createSheet("Lehrkräfte");

    }

    private void writeHeaderRow(){

        XSSFFont font= workbook.createFont();
        font.setBold(true);

        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setFont(font);

        Row row = sheet.createRow(0);

        Cell cell = row.createCell(0);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Personalnummer");

        cell = row.createCell(1);
        cell.setCellValue("Geschlecht");

        cell = row.createCell(2);
        cell.setCellValue("Grad");

        cell = row.createCell(3);
        cell.setCellValue("Vorname");

        cell = row.createCell(4);
        cell.setCellValue("Nachname");

        cell = row.createCell(5);
        cell.setCellValue("E-Mail");

        cell = row.createCell(6);
        cell.setCellValue("Telefon");

        cell = row.createCell(7);
        cell.setCellValue("Strasse");

        cell = row.createCell(8);
        cell.setCellValue("Postleitzahl");

        cell = row.createCell(9);
        cell.setCellValue("Fächer");

        cell = row.createCell(10);
        cell.setCellValue("Schulart");

        cell = row.createCell(11);
        cell.setCellValue("Schulname");

        cell = row.createCell(12);
        cell.setCellValue("Schulnummer");

    }

    private void writeDateRows(){

        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setAlignment(HorizontalAlignment.CENTER);

        for (Teacher teacher: listTeacher) {

            Row row = sheet.createRow(rowIndex);
            rowIndex++;

            Cell cell = row.createCell(0);
            cell.setCellValue(teacher.getPersonal_number());

            cell = row.createCell(1);
            cell.setCellValue(teacher.getGender());

            cell = row.createCell(2);
            cell.setCellValue(teacher.getGrad());

            cell = row.createCell(3);
            cell.setCellValue(teacher.getFirstname());

            cell = row.createCell(4);
            cell.setCellValue(teacher.getLastname());

            cell = row.createCell(5);
            cell.setCellValue(teacher.getEmail());

            cell = row.createCell(6);
            cell.setCellValue(teacher.getTelephone());

            cell = row.createCell(7);
            cell.setCellValue(teacher.getStreet());

            cell = row.createCell(8);
            cell.setCellValue(teacher.getZip());

            cell = row.createCell(9);
            cell.setCellStyle(cellStyle);
            cell.setCellValue(teacher.getCity());

            cell = row.createCell(10);
            cell.setCellStyle(cellStyle);
            cell.setCellValue(teacher.getSchool_branch());

            cell = row.createCell(11);
            cell.setCellValue(teacher.getSchool_name());

            cell = row.createCell(12);
            cell.setCellValue(teacher.getSchool_number());

        }
        sheet.autoSizeColumn(0);
        sheet.autoSizeColumn(1);
        sheet.autoSizeColumn(2);
        sheet.autoSizeColumn(3);
        sheet.autoSizeColumn(4);
        sheet.autoSizeColumn(5);
        sheet.autoSizeColumn(6);
        sheet.autoSizeColumn(7);
        sheet.autoSizeColumn(8);
        sheet.autoSizeColumn(9);
        sheet.autoSizeColumn(10);
        sheet.autoSizeColumn(11);
        sheet.autoSizeColumn(12);

    }

    public ResponseEntity<InputStreamResource> export() throws IOException {
        writeHeaderRow();
        writeDateRows();

        File file = new File(getClass().getResource(".").getFile() + "/teachers.xlsx");
        file.createNewFile();
        FileOutputStream outputStream = new FileOutputStream(file, false);
        workbook.write(outputStream);
        workbook.close();
        outputStream.close();

        String headerKey = "Content-Disposition";
        String headerValue = "attachement; filename=lehrkraefte.xlsx";

        InputStreamResource resource = new InputStreamResource(new FileInputStream(file));

        return ResponseEntity.ok().header(headerKey, headerValue)
                .contentLength(file.length())
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(resource);
    }

}
