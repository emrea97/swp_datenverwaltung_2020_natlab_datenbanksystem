package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class AuthRequest {

    private String username;
    private String password;
    private long expiresIn;

    public String getUserName() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public long  getExpiresIn() { return expiresIn; }
}