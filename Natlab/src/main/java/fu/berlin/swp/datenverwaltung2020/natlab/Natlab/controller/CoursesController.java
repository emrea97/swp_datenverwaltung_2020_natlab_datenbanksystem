package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity.Course;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.export.CourseExcelExporter;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.repository.CourseRepository;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.request.CourseRequest;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.response.CourseResponse;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

@RestController
public class CoursesController {

    @Autowired
    private CourseRepository courserRepository;

    @PostMapping("/insertCourse")
    public ResponseEntity<?> editCourse(@RequestBody CourseRequest coursesRequest) {

        List<Course> courses = new ArrayList<>();
        Course course = new Course();

        course.setCourse_number(coursesRequest.getCourse_number());
        course.setCourse_offer(coursesRequest.getCourse_offer());
        course.setCourse_name(coursesRequest.getCourse_name());
        course.setEvent_type(coursesRequest.getEvent_type());
        course.setCourse_datetime(coursesRequest.getCourse_datetime());
        course.setCourse_topic(coursesRequest.getCourse_topic());
        course.setCourse_location(coursesRequest.getCourse_location());
        course.setAccompanyingTeacher(coursesRequest.getAccompanyingTeacher());
        course.setSupervisingStudents(coursesRequest.getSupervisingStudents());
        course.setPresentStudents(coursesRequest.getPresentStudents());
        course.setRegisteredStudents(coursesRequest.getRegisteredStudents());
        course.setInstructor(coursesRequest.getInstructor());
        course.setNoteField(coursesRequest.getNoteField());
        course.setHonorar(coursesRequest.getHonorar());
        course.setAnzahlung(coursesRequest.getAnzahlung());
        course.setNoteField(coursesRequest.getNoteField());
        course.setStatus(1);

        courses.add(course);
        courserRepository.saveAll(courses);

        return ResponseEntity.ok(new CourseResponse(1));

    }

    @GetMapping("/getCourses")
    public List<Course> getCourses() {

        return courserRepository.findAll();

    }

    @GetMapping(path = "/getCoursesByID/{id}")
    public Course getCourses(@PathVariable("id") int id) {

        return courserRepository.findAllById(id).get(0);

    }

    @PostMapping("/updateCourse")
    public int updateCourse(@RequestBody CourseRequest request) {

        courserRepository.updateCourse(request.getId(), request.getCourse_number(), request.getCourse_offer(), request.getCourse_name(), request.getEvent_type(),request.getCourse_datetime(), request.getCourse_topic(), request.getCourse_location(), request.getAccompanyingTeacher(), request.getSupervisingStudents(), request.getPresentStudents(), request.getRegisteredStudents(), request.getInstructor(), request.getHonorar(), request.getAnzahlung(), request.getNoteField());

        return 1;

    }

    @PostMapping("/deleteCourse")
    public int deleteCourse(@RequestBody CourseRequest request) {

        courserRepository.updateStatusById(request.getId());

        return 1;

    }

    @GetMapping(path = "/exportCourseExcel/{ids}")
    public ResponseEntity<?> exportExcel(@PathVariable("ids") String[] ids) throws IOException {

            File file = new File(getClass().getResource(".").getFile() + "/test.xlsx");
            file.createNewFile();
            List<Course> courses;

            ArrayList<Integer> servs = new ArrayList<>();
            for (String id : ids) {
                servs.add(Integer.parseInt(id));
            }

            courses = courserRepository.findAllById(servs);

            CourseExcelExporter excelExporter = new CourseExcelExporter(courses);

            return excelExporter.export();

    }

    @GetMapping(path = "/exportCourseExcel")
    public ResponseEntity<?> exportExcelAll() throws IOException {

            File file = new File(getClass().getResource(".").getFile() + "/test.xlsx");
            file.createNewFile();
            List<Course> courses = courserRepository.findAll();

            CourseExcelExporter excelExporter = new CourseExcelExporter(courses);

            return excelExporter.export();

    }


}
