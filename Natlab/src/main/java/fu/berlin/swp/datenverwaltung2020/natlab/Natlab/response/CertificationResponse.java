package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.response;

import lombok.Getter;

@Getter
public class CertificationResponse {

    private int status;

    public CertificationResponse(int status) {
        this.status = status;

    }

}
