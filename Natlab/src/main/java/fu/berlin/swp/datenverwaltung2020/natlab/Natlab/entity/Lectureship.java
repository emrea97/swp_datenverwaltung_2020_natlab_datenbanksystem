package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.annotation.processing.Generated;
import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "lectureships")
public class Lectureship {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "ifd_nr")
    private String ifd;

    @Column(name = "anrede")
    private String anrede;

    @Column(name = "grad")
    private String grad;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "surname")
    private String surname;

    @Column(name = "street")
    private String street;

    @Column(name = "postcode")
    private String postcode;

    @Column(name = "ort")
    private String ort;

    @Column(name = "additional_address")
    private String additional_address;

    @Column(name = "lv_title")
    private String lvTitle;

    @Column(name = "lv_type")
    private String lvType;

    @Column(name = "internal_external")
    private String internalExternal;

    @Column(name = "verguetung")
    private String verguetung;

    @Column(name = "unzulaessige_Nebentaetigkeit")
    private String unzulaessigeNebentaetigkeit;

    @Column(name = "requested_by")
    private String requestedBy;

    @Column(name = "requested_at")
    private String requestedAt;

    @Column(name = "approved_am")
    private String approvedAm;

    @Column(name = "seh")
    private String seh;

    @Column(name = "std_satz")
    private String StdSatz;

    @Column(name = "bewilligtes_honorar")
    private String bewilligtesHonorar;

    @Column(name = "abschlag_01")
    private String abschlag01;

    @Column(name = "abschlag_02")
    private String abschlag02;

    @Column(name = "abschlag_03")
    private String abschlag03;

    @Column(name = "abschlag_04")
    private String abschlag04;

    @Column(name = "gezahltes_honorar")
    private String gezahltesHonorar;

    @Column(name = "booked")
    private String booked;

    @Column(name = "kst")
    private String kst;

    @Column(name = "fond")
    private String fond;

    @Column(name = "bemerkung")
    private String bemerkung;

    @Column(name = "new")
    private String neu;

    @Column(name = "e_er")
    private String eEr;

    @Column(name = "grad_Anschr")
    private String grad_Anschr;

    @Column(name = "_in")
    private String _in;

    @Column(name = "comment")
    private String comment;

    @Column(name="year")
    private String year;

    @ManyToMany
    @JoinTable(name = "users_lectureships",
            joinColumns = @JoinColumn(name = "id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    private List<User> users;

    @ManyToMany(mappedBy = "lectureships")
    private List<Course> courses;
}
