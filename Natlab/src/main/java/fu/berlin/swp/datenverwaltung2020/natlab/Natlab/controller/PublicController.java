package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PublicController {

    @GetMapping("/hello")
    public String welcome(){
        return "Welcome to Natlab !!!";
    }

}
