package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.response;

public class RegisterResponse {

    private final String firstname;
    private final String name;
    private final String username;
    private final String password;
    private final String email;

    public RegisterResponse(String firstname, String name, String username, String password, String email) {
        this.firstname = firstname;
        this.name = name;
        this.username = username;
        this.password = password;
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getName() {
        return name;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }
}
