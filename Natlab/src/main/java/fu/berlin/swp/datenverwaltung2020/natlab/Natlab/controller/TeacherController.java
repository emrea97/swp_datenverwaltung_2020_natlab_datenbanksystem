package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.controller;

import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity.Course;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity.Teacher;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.export.CourseExcelExporter;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.export.TeacherExcelExporter;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.repository.CourseRepository;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.repository.TeacherRepository;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.request.CourseRequest;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.request.TeacherRequest;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.response.CourseResponse;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.response.TeacherResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.TabExpander;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
public class TeacherController {

    @Autowired
    private TeacherRepository teacherRepository;

    @PostMapping("/insertTeacher")
    public ResponseEntity<?> insertTeacher(@RequestBody TeacherRequest request) {
        List<Teacher> teachers = new ArrayList<>();
        Teacher teacher = new Teacher();
        teacher.setSchool_branch(request.getSchool_branch());
        teacher.setGrad(request.getGrad());
        teacher.setFirstname(request.getFirstname());
        teacher.setLastname(request.getLastname());
        teacher.setGender(request.getGender());
        teacher.setSubject(request.getSubject());
        teacher.setPersonal_number(request.getPersonal_number());
        teacher.setEmail(request.getEmail());
        teacher.setSchool_name(request.getSchool_name());
        teacher.setSchool_number(request.getSchool_number());
        teacher.setTelephone(request.getTelephone());
        teacher.setStreet(request.getStreet());
        teacher.setZip(request.getZip());
        teacher.setCity(request.getCity());
        teacher.setStatus(1);

        teachers.add(teacher);

        teacherRepository.saveAll(teachers);

        return ResponseEntity.ok(teacher);

    }

    @GetMapping("/getTeachers")
    public List<Teacher> getTeachers() {

        return teacherRepository.findAll();

    }

    @GetMapping(path = "/getTeacherByID/{id}")
    public Teacher getTeacher(@PathVariable("id") int id) {

        return teacherRepository.findAllById(id).get(0);

    }

    @PostMapping("/updateTeacher")
    public int updateTeacher(@RequestBody TeacherRequest request) {

        teacherRepository.updateTeacher(request.getId(), request.getSchool_branch(), request.getGrad(), request.getFirstname(), request.getLastname(),request.getGender(),request.getSubject(), request.getPersonal_number(), request.getEmail(), request.getSchool_name(), request.getSchool_number(), request.getTelephone(), request.getStreet(), request.getZip(), request.getCity(), request.getStatus());

        return 1;

    }

    @PostMapping("/deleteTeacher")
    public int deleteTeacher(@RequestBody TeacherRequest request) {

        teacherRepository.updateStatusById(request.getId());

        return 1;

    }

    @GetMapping(path = "/exportTeacherExcel/{ids}")
    public ResponseEntity<?> exportExcel(@PathVariable("ids") String[] ids) throws IOException {

        File file = new File(getClass().getResource(".").getFile() + "/Lehrkräfte.xlsx");
        file.createNewFile();
        List<Teacher> teachers;

        ArrayList<Integer> servs = new ArrayList<>();
        for (String id : ids) {
            servs.add(Integer.parseInt(id));
        }

        teachers = teacherRepository.findAllById(servs);

        TeacherExcelExporter excelExporter = new TeacherExcelExporter(teachers);

        return excelExporter.export();

    }

    @GetMapping(path = "/exportTeacherExcel")
    public ResponseEntity<?> exportExcelAll() throws IOException {

        File file = new File(getClass().getResource(".").getFile() + "/Lehrkräfte.xlsx");
        file.createNewFile();
        List<Teacher> teachers = teacherRepository.findAll();

        TeacherExcelExporter excelExporter = new TeacherExcelExporter(teachers);

        return excelExporter.export();

    }

}
