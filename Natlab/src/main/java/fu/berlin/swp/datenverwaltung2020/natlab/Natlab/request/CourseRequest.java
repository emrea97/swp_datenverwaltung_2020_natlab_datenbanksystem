package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class CourseRequest {

    private int id;
    private String course_number;
    private String course_offer;
    private String course_name;
    private String event_type;
    private String course_datetime;
    private String course_topic;
    private String course_location;
    private String accompanyingTeacher;
    private String supervisingStudents;
    private int presentStudents;
    private int registeredStudents;
    private String instructor;
    private String noteField;
    private String anzahlung;
    private String honorar;
    private String status;

}