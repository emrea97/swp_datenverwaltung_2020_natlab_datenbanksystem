/*
package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.service;

import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity.Course;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity.Lectureship;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity.Student;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity.Teacher;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.repository.CourseRepository;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.repository.LectureshipRepository;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.repository.StudentRepository;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.repository.TeacherRepository;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class DataImportService {

    @Autowired
    private TeacherRepository teacherRepo;

    @Autowired
    private LectureshipRepository lectureshipRepo;

    @Autowired
    private StudentRepository studentRepo;

    @Autowired
    private CourseRepository courserRepository;
/*
    @PostConstruct @Transactional
    public void importLectureship() {
        try {
            //FileInputStream fis = new FileInputStream(new File("src/main/resources/LectureshipData.xls"));
            InputStream fis2 = getClass().getResourceAsStream("/LectureshipData.xls");
            HSSFWorkbook wb = new HSSFWorkbook(fis2);
            //creating a Sheet object to retrieve the object
            int numberOfSheets = wb.getNumberOfSheets();
            List<Lectureship> lectureships = new ArrayList<>();
            // for loop for sheets
            for (int i = 0; i < 2; i++) {
                HSSFSheet sheet = wb.getSheetAt(i);
                System.out.println(sheet.getSheetName());
                String jahr = sheet.getSheetName().split(" ")[1];
                for (int j = 1; j < sheet.getPhysicalNumberOfRows() && sheet.getRow(j).getCell(0) != null; j++) {
                    Row row = sheet.getRow(j);
                    Lectureship lectureship = new Lectureship();
                    lectureship.setIfd(row.getCell(0).getStringCellValue());
                    lectureship.setAnrede(row.getCell(1).getStringCellValue());
                    row.getCell(2).setCellType(CellType.STRING);
                    lectureship.setGrad(row.getCell(2).getStringCellValue());
                    lectureship.setFirstName(row.getCell(3).getStringCellValue());
                    lectureship.setSurname(row.getCell(4).getStringCellValue());
                    lectureship.setStreet(row.getCell(5).getStringCellValue());
                    row.getCell(6).setCellType(CellType.STRING);
                    lectureship.setPostcode(row.getCell(6).getStringCellValue());
                    lectureship.setOrt(row.getCell(7).getStringCellValue());
                    lectureship.setAdditional_address(row.getCell(8).getStringCellValue());
                    lectureship.setLvTitle(row.getCell(9).getStringCellValue());
                    lectureship.setLvType(row.getCell(10).getStringCellValue());
                    row.getCell(11).setCellType(CellType.STRING);
                    lectureship.setInternalExternal(row.getCell(11).getStringCellValue());
                    row.getCell(12).setCellType(CellType.STRING);
                    lectureship.setVerguetung(row.getCell(12).getStringCellValue());
                    lectureship.setUnzulaessigeNebentaetigkeit(row.getCell(13).getStringCellValue());
                    lectureship.setRequestedBy(row.getCell(14).getStringCellValue());
                    if (row.getCell(15) != null)
                        row.getCell(15).setCellType(CellType.STRING);
                    lectureship.setRequestedAt(row.getCell(15) != null ? row.getCell(15).getStringCellValue() : "");
                    if (row.getCell(16) != null)
                        row.getCell(16).setCellType(CellType.STRING);
                    lectureship.setApprovedAm(row.getCell(16) != null ? row.getCell(16).getStringCellValue() : "");
                    lectureship.setSeh(Double.toString(row.getCell(17).getNumericCellValue()));
                    lectureship.setStdSatz(Double.toString(row.getCell(18).getNumericCellValue()));
                    lectureship.setBewilligtesHonorar(Double.toString(Double.parseDouble(lectureship.getSeh()) * Double.parseDouble(lectureship.getStdSatz())));
                    lectureship.setAbschlag01(Double.toString(row.getCell(20).getNumericCellValue()));
                    lectureship.setAbschlag02(Double.toString(row.getCell(21).getNumericCellValue()));
                    lectureship.setAbschlag03(Double.toString(row.getCell(22).getNumericCellValue()));
                    System.out.println(row.getCell(23).getRowIndex());
                    lectureship.setGezahltesHonorar(lectureship.getAbschlag01() != null ? Double.toString(Double.parseDouble(lectureship.getAbschlag01()) + Double.parseDouble(lectureship.getAbschlag02())) : "");
                    if (row.getCell(24) != null)
                        row.getCell(24).setCellType(CellType.STRING);
                    lectureship.setBooked(row.getCell(24).getStringCellValue());
                    if (row.getCell(25) != null)
                        row.getCell(25).setCellType(CellType.STRING);
                    lectureship.setKst(row.getCell(25).getStringCellValue());
                    lectureship.setFond(row.getCell(26).getNumericCellValue() != 0.0d ? Double.toString(row.getCell(26).getNumericCellValue()).split("\\.")[0] : null);
                    lectureship.setNeu(row.getCell(27) != null && row.getCell(27).getNumericCellValue() == 1.0d ? "1" : "0");
                    lectureship.setEEr(lectureship.getAnrede().equals("Frau") ? "e" : "er");
                    lectureship.setGrad_Anschr(lectureship.getGrad().contains("Prof.") ? "Professor" : lectureship.getGrad().contains("Dr.") ? "Dr." : "");
                    lectureship.set_in(lectureship.getEEr().equals("e") ? "in" : "");
                    lectureship.setComment(row.getCell(31).getStringCellValue());
                    lectureship.setYear(sheet.getSheetName().split(" ")[1]);
                    lectureships.add(lectureship);
                }
            }

            lectureshipRepo.saveAll(lectureships);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @PostConstruct @Transactional
    public void importStudent() {
        try {
            //FileInputStream fis = new FileInputStream(new File("src/main/resources/StudentData.xls"));
            InputStream fis2 = getClass().getResourceAsStream("/StudentData.xls");
            HSSFWorkbook wb = new HSSFWorkbook(fis2);
            //creating a Sheet object to retrieve the object
            HSSFSheet sheet = wb.getSheetAt(0);
            List<Student> students = new ArrayList<>();
            //evaluating cell type
            FormulaEvaluator formulaEvaluator = wb.getCreationHelper().createFormulaEvaluator();

            for (int i = 1; i < sheet.getPhysicalNumberOfRows() && sheet.getRow(i).getCell(0) != null; i++) {
                Row row = sheet.getRow(i);
                Student student = new Student();
                student.setAnrede(row.getCell(0).getStringCellValue());
                student.setGrad(row.getCell(1).getStringCellValue());
                student.setFirstName(row.getCell(2).getStringCellValue());
                student.setSurname(row.getCell(3).getStringCellValue());
                student.setStreet(row.getCell(4).getStringCellValue());
                row.getCell(5).setCellType(CellType.STRING);
                student.setPostcode(row.getCell(5).getStringCellValue());
                student.setOrt(row.getCell(6).getStringCellValue());
                student.setAdditional_address(row.getCell(7).getStringCellValue());
                student.setPersonnel_questionnaire(row.getCell(8).getStringCellValue());
                if (row.getCell(9) != null)
                    row.getCell(9).setCellType(CellType.STRING);
                student.setSummerUni_17(row.getCell(9) != null ? row.getCell(9).getStringCellValue() : "");

                if (row.getCell(10) != null)
                    row.getCell(10).setCellType(CellType.STRING);
                student.setSummerUni_18(row.getCell(10) != null ? row.getCell(10).getStringCellValue() : "");

                if (row.getCell(11) != null)
                    row.getCell(11).setCellType(CellType.STRING);
                student.setSummerUni_19(row.getCell(11) != null ? row.getCell(11).getStringCellValue() : "");

                if (row.getCell(12) != null)
                    row.getCell(12).setCellType(CellType.STRING);
                student.setSummerUni_20(row.getCell(12) != null ? row.getCell(12).getStringCellValue() : "");

                student.setNatuerlich(row.getCell(13).getStringCellValue());
                students.add(student);
            }

            studentRepo.saveAll(students);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @PostConstruct @Transactional
    public void importTeacher() {
        try {
            //FileInputStream fis = new FileInputStream(new File("src/main/resources/TeacherData.xls"));
            InputStream fis2 = getClass().getResourceAsStream("/TeacherData.xls");
            HSSFWorkbook wb = new HSSFWorkbook(fis2);
            //creating a Sheet object to retrieve the object
            HSSFSheet sheet = wb.getSheetAt(0);
            List<Teacher> teachers = new ArrayList<>();
            //evaluating cell type
            FormulaEvaluator formulaEvaluator = wb.getCreationHelper().createFormulaEvaluator();

            for (int i = 1; i < sheet.getPhysicalNumberOfRows() && sheet.getRow(i).getCell(0) != null; i++) {
                Row row = sheet.getRow(i);
                Teacher teacher = new Teacher();
                teacher.setSchool_branch(row.getCell(0).getStringCellValue());
                teacher.setGrad(row.getCell(1).getStringCellValue());
                teacher.setLastname(row.getCell(2).getStringCellValue());
                teacher.setFirstname(row.getCell(3).getStringCellValue());
                teacher.setGender(row.getCell(4).getStringCellValue());
                //teacher.setIng_id(Double.parseDouble(String.valueOf(row.getCell(5).getNumericCellValue())));
                teacher.setEmail(row.getCell(6).getStringCellValue());
                //teacher.setStatus(row.getCell(7).getBooleanCellValue());
                teacher.setStatus(1);
                teacher.setSchool_name(row.getCell(8).getStringCellValue());
                //teacher.setSchoolDistrict(row.getCell(9).getStringCellValue());
                //teacher.setAusdr1(row.getCell(10) != null && row.getCell(10).getNumericCellValue() == 0.0d ? "0" : "-1");
                teachers.add(teacher);

            }
            teacherRepo.saveAll(teachers);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @PostConstruct @Transactional
    public void importCourse() {
        try {
            //FileInputStream fis = new FileInputStream(new File("../src/main/resources/CourseData.xls"));
            InputStream fis2 = getClass().getResourceAsStream("/CourseData.xls");

            HSSFWorkbook wb = new HSSFWorkbook(fis2);
            //creating a Sheet object to retrieve the object
            int numberOfSheets = wb.getNumberOfSheets();
            List<Course> courses = new ArrayList<>();
            // for loop for sheets
            for (int i = 0; i < numberOfSheets; i++) {
                HSSFSheet sheet = wb.getSheetAt(i);
                System.out.println(sheet.getSheetName());
                String jahr = sheet.getSheetName().split(" ")[1];
                for (int j = 1; j < sheet.getPhysicalNumberOfRows() && sheet.getRow(j).getCell(0) != null; j++) {
                    Row row = sheet.getRow(j);
                    Course course = new Course();
                    course.setCourse_number(row.getCell(0).getStringCellValue());
                    course.setCourse_offer(row.getCell(1).getStringCellValue());
                    if (row.getCell(2) != null)
                        row.getCell(2).setCellType(CellType.STRING);
                    //course.setDay(row.getCell(2).getStringCellValue());
                    if (row.getCell(3) != null)
                        row.getCell(3).setCellType(CellType.STRING);
                    //course.setTime(row.getCell(3).getStringCellValue());
                    course.setCourse_location(row.getCell(4).getStringCellValue());
                    if (row.getCell(5) != null)
                        row.getCell(5).setCellType(CellType.STRING);
                    course.setCourse_datetime(row.getCell(5).getStringCellValue());
                    //course.setStudGEB(row.getCell(6).getNumericCellValue());
                    //course.setMitGEB(row.getCell(7).getNumericCellValue());
                    //course.setGastGEB(row.getCell(8).getNumericCellValue());
                    //course.setStatus4GEB(row.getCell(9).getNumericCellValue());
                    //course.setAnzahlung(row.getCell(10).getNumericCellValue());
                    //course.setHonorar(row.getCell(11).getNumericCellValue());
                    if (row.getCell(12) != null)
                        row.getCell(12).setCellType(CellType.STRING);
                    course.setInstructor(row.getCell(12).getStringCellValue());
                    //course.setKapazitaet((row.getCell(13).getNumericCellValue()));
                    //course.setAnmeldungen(row.getCell(14).getNumericCellValue());
                    if (row.getCell(15) != null)
                        row.getCell(15).setCellType(CellType.STRING);
                    //course.setBereich(row.getCell(15).getStringCellValue());
                    course.setNoteField(row.getCell(16).getStringCellValue());
                    //course.setYear(sheet.getSheetName().split(" ")[1]);
                    course.setStatus(1);
                    courses.add(course);
                }
            }
            courserRepository.saveAll(courses);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
*/