package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.response;

import lombok.Getter;

@Getter
public class TeacherResponse {

    private int status;

    public TeacherResponse(int status) {
        this.status = status;

    }

}
