package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.response;

import lombok.Getter;

@Getter
public class StudentResponse {

    private int status;

    public StudentResponse(int status) {
        this.status = status;

    }

}
