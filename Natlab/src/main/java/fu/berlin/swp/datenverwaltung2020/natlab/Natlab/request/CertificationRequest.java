package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class CertificationRequest {

    private Integer id;
    private String certifications_id;
    private String certificationtype;
    private String certificationname;
    private String certificationowner;
    private String certificationdate;
    private String certificationexpiredate;
    private Integer status;

}