package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.controller;

import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.request.AuthRequest;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.request.RegisterRequest;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity.User;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.repository.UserRepository;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.response.AuthenticationResponse;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin
public class UserController {

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @PostMapping("/register")
    public ResponseEntity<?> registerUser(@RequestBody RegisterRequest registerRequest) {
        List<User> users = new ArrayList<>();
        User user = new User(registerRequest.getfirstname(), registerRequest.getname(), registerRequest.getUsername(), registerRequest.getEmail(), passwordEncoder.encode(registerRequest.getPassword()));
        users.add(user);

        userRepository.saveAll(users);

        return ResponseEntity.ok(user);

    }

    @PostMapping("/authenticate")
    public ResponseEntity<?> authUser(@RequestBody AuthRequest authRequest) throws Exception {

        try {
            authenticationManager.authenticate( new UsernamePasswordAuthenticationToken(authRequest.getUserName(), authRequest.getPassword()) );
        } catch (Exception ex) {
            throw new Exception("inavalid username/password");
        }

        final String jwt = jwtUtil.generateToken(authRequest.getUserName());
        final long expiration = jwtUtil.extractExpiration(jwt).getTime();
        int userid = userRepository.findByUserName(authRequest.getUserName()).getId();

        return ResponseEntity.ok(new AuthenticationResponse(jwt, expiration, userid ));

    }

    @PostMapping("/isExpired")
    public ResponseEntity<?> isExpired(@RequestBody AuthRequest authRequest){

        long currentTime = System.currentTimeMillis();

        if (currentTime >= authRequest.getExpiresIn())
            return ResponseEntity.ok(new AuthenticationResponse(0));
        else
            return ResponseEntity.ok(new AuthenticationResponse(1));
    }

}
