package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class TeacherRequest {

    private Integer id;
    private String  school_branch;
    private String  grad;
    private String  firstname;
    private String  lastname;
    private String  gender;
    private String  subject;
    private String  personal_number;
    private String  email;
    private String  school_name;
    private Integer school_number;
    private String  telephone;
    private String  street;
    private Integer zip;
    private String  city;
    private Integer status;

}