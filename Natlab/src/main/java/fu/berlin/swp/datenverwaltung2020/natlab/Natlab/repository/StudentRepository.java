package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.repository;

import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity.Student;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;


@Repository
public interface StudentRepository extends JpaRepository<Student, Integer> {

    List<Student> findAllById(int id);

    @Transactional
    @Modifying
    @Query("FROM Student s WHERE s.status = 1")
    List<Student> findAll();

    @Transactional @Modifying
    @Query("UPDATE Student s SET s.anrede = :anrede, s.firstName = :firstname, s.lastname = :lastname, s.street = :street, s.postcode = :zip, s.ort = :city, s.summerUni_17 = :sommeruni17, s.summerUni_18 = :sommeruni18, s.summerUni_19 = :sommeruni19, s.summerUni_20 = :sommeruni20, s.natuerlich = :natuerlich_20, s.status = :status WHERE s.id = :studentId")
    void updateStudent(@Param("studentId") int id, @Param("anrede") String anrede, @Param("firstname") String firstname, @Param("lastname") String lastname, @Param("street") String street, @Param("zip") String zip, @Param("city") String city, @Param("sommeruni17") String sommeruni17, @Param("sommeruni18") String sommeruni18, @Param("sommeruni19") String sommeruni19, @Param("sommeruni20") String sommeruni20, @Param("natuerlich_20") String natuerlich_20, @Param("status") int status);

    @Transactional @Modifying
    @Query("UPDATE Student s SET s.status = 0 WHERE s.id = :studentId")
    void updateStatusById(@Param("studentId") Integer id);

}
