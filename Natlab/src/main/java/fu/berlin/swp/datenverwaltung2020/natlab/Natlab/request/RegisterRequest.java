package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class RegisterRequest {

    private String firstname;
    private String name;
    private String username;
    private String password;
    private String email;

    public String getfirstname() {
        return firstname;
    }

    public String getname() {
        return name;
    }

    public String getUsername() { return username; }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }
}