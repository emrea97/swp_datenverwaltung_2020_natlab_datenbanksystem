package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.export;

import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity.Course;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity.Student;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity.Teacher;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class StudentExcelExporter {

    private final XSSFWorkbook workbook;
    private final XSSFSheet sheet;

    private final List<Student> listStudent;
    private int rowIndex = 1;

    public StudentExcelExporter(List<Student> listStudent) {
        this.listStudent = listStudent;
        workbook = new XSSFWorkbook();
        sheet = workbook.createSheet("Studenten");

    }

    private void writeHeaderRow(){

        XSSFFont font= workbook.createFont();
        font.setBold(true);

        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setFont(font);

        Row row = sheet.createRow(0);

        Cell cell = row.createCell(0);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Anrede");

        cell = row.createCell(1);
        cell.setCellValue("Vorname");

        cell = row.createCell(2);
        cell.setCellValue("Nachname");

        cell = row.createCell(3);
        cell.setCellValue("Straße");

        cell = row.createCell(4);
        cell.setCellValue("Postleitzahl");

        cell = row.createCell(5);
        cell.setCellValue("Ort");

        cell = row.createCell(6);
        cell.setCellValue("SommerUni 17");

        cell = row.createCell(7);
        cell.setCellValue("SommerUni 18");

        cell = row.createCell(8);
        cell.setCellValue("SommerUni 19");

        cell = row.createCell(9);
        cell.setCellValue("SommerUni 20");

        cell = row.createCell(10);
        cell.setCellValue("Natürlich 20");
    }

    private void writeDateRows(){

        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setAlignment(HorizontalAlignment.CENTER);

        for (Student student: listStudent) {

            Row row = sheet.createRow(rowIndex);
            rowIndex++;

            Cell cell = row.createCell(0);
            cell.setCellValue(student.getAnrede());

            cell = row.createCell(1);
            cell.setCellValue(student.getFirstName());

            cell = row.createCell(2);
            cell.setCellValue(student.getLastname());

            cell = row.createCell(3);
            cell.setCellValue(student.getStreet());

            cell = row.createCell(4);
            cell.setCellValue(student.getPostcode());

            cell = row.createCell(5);
            cell.setCellValue(student.getOrt());

            cell = row.createCell(6);
            cell.setCellValue(student.getSummerUni_17());

            cell = row.createCell(7);
            cell.setCellValue(student.getSummerUni_18());

            cell = row.createCell(8);
            cell.setCellValue(student.getSummerUni_19());

            cell = row.createCell(9);
            cell.setCellStyle(cellStyle);
            cell.setCellValue(student.getSummerUni_20());

            cell = row.createCell(10);
            cell.setCellStyle(cellStyle);
            cell.setCellValue(student.getNatuerlich());

        }
        sheet.autoSizeColumn(0);
        sheet.autoSizeColumn(1);
        sheet.autoSizeColumn(2);
        sheet.autoSizeColumn(3);
        sheet.autoSizeColumn(4);
        sheet.autoSizeColumn(5);
        sheet.autoSizeColumn(6);
        sheet.autoSizeColumn(7);
        sheet.autoSizeColumn(8);
        sheet.autoSizeColumn(9);
        sheet.autoSizeColumn(10);

    }

    public ResponseEntity<InputStreamResource> export() throws IOException {
        writeHeaderRow();
        writeDateRows();

        File file = new File(getClass().getResource(".").getFile() + "/students.xlsx");
        file.createNewFile();
        FileOutputStream outputStream = new FileOutputStream(file, false);
        workbook.write(outputStream);
        workbook.close();
        outputStream.close();

        String headerKey = "Content-Disposition";
        String headerValue = "attachement; filename=students.xlsx";

        InputStreamResource resource = new InputStreamResource(new FileInputStream(file));

        return ResponseEntity.ok().header(headerKey, headerValue)
                .contentLength(file.length())
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(resource);
    }

}
