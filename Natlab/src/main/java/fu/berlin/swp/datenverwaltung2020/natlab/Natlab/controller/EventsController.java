package fu.berlin.swp.datenverwaltung2020.natlab.Natlab.controller;

import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.entity.Event;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.repository.EventRepository;
import fu.berlin.swp.datenverwaltung2020.natlab.Natlab.request.EventRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class EventsController {

    @Autowired
    EventRepository eventRepository;

    @PostMapping("/insertEvent")
    public ResponseEntity<?> insertEvent(@RequestBody EventRequest eventRequest) {

        List<Event> events = new ArrayList<>();
        Event event = new Event();

        event.setTitle(eventRequest.getTitle());
        event.setDescription(eventRequest.getDescription());
        event.setStartTime(eventRequest.getStartTime());
        event.setEndTime(eventRequest.getEndTime());
        event.setStatus(1);

        events.add(event);

        eventRepository.saveAll(events);

        return ResponseEntity.ok(1);
    }

    @GetMapping("/getEvents")
    public List<Event> getEvents() {

        return eventRepository.findAll();

    }
    @GetMapping("/deleteEvent/{id}")
    public ResponseEntity<Integer> getEvents(@PathVariable("id") int id) {

        eventRepository.updateStatusById(id);

        return ResponseEntity.ok(1);

    }
}
